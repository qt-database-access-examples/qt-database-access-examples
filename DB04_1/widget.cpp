#include "widget.h"
#include "ui_widget.h"

#include <QSqlTableModel>
#include <QSqlRecord>
#include <QModelIndex>

// Констуктор класса Widget
Widget::Widget(QWidget *parent) :
        QWidget(parent),
        ui(new Ui::Widget)
{
    ui->setupUi(this);
    
    // Создаем модели
    groupsModel = new QSqlTableModel;
    studlistModel = new QSqlTableModel;
    
    // Задаем таблицы
    groupsModel->setTable("groups");
    studlistModel->setTable("studlist");
    
    groupsModel->select();
    studlistModel->select();
    
    ui->groupsView->setModel(groupsModel);
    ui->studlistView->setModel(studlistModel);
    
    ui->groupsView->hideColumn(0);
    ui->studlistView->hideColumn(0);
    ui->studlistView->hideColumn(2);
    
    connect(ui->groupsView->selectionModel(), SIGNAL(currentChanged(QModelIndex,QModelIndex)),
            this, SLOT(groupChanged(QModelIndex)));
    
}

Widget::~Widget()
{
    // Перед разрушением виджета отправляем все данные в БД
    groupsModel->submitAll();
    studlistModel->submitAll();
    
    delete ui;
}

void Widget::groupChanged(const QModelIndex &index)
{
    if (index.isValid())
    {
        QSqlRecord rec = groupsModel->record(index.row());
        int id_group = rec.value("id_group").toInt();
        QString filter = QString("id_group = %1").arg(id_group);
        studlistModel->setFilter(filter);
    } else
    {
        studlistModel->setFilter("id_group = -1");
    }
    studlistModel->select();
}

void Widget::on_buttonAddStudent_clicked()
{
    // Если поле ФИО пустое, выходим из функции
    if (ui->lineEdit->text().length() == 0) return;
    
    // Вычисляем текущий индекс выбранной группы
    QModelIndex index = ui->groupsView->currentIndex();
    
    // Если индекс невалидный - выходим из функции
    if (!index.isValid()) return;
    
    // Вычисляем значение id_group через индекс
    QSqlRecord rec = groupsModel->record(index.row());
    int id_group = rec.value("id_group").toInt();
    
    // Сохраняем ФИО из текстового поля в переменную fio    
    QString fio = ui->lineEdit->text();
    
    // Добавляем строку в модель studlistModel
    int row = 0;
    studlistModel->insertRow(row);
    
    // Вычисляем индексы столбцов fio и id_group
    QModelIndex fioCol = studlistModel->index(row, 1);
    QModelIndex id_groupCol = studlistModel->index(row, 2);
    
    // Заносим данные в столбцы fio и id_group
    studlistModel->setData(fioCol, fio);    
    studlistModel->setData(id_groupCol, id_group);
    
    // Отправляем все данные в БД
    studlistModel->submitAll();
    
    // Очищаем поле
    ui->lineEdit->setText("");
}
