#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

class QSqlTableModel; // Предварительное объявление класса QSqlTableModel
class QModelIndex; // Предварительное объявление класса QModelIndex

namespace Ui {
    class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private:
    Ui::Widget *ui;
    QSqlTableModel* groupsModel; // Модель для таблицы groups
    QSqlTableModel* studlistModel; // Модель для таблицы studlist
    
private slots:
    void on_buttonAddStudent_clicked();
    void groupChanged(const QModelIndex&);
};

#endif // WIDGET_H
