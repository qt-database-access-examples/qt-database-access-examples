#include "widget.h"
#include "ui_widget.h"
#include <QSqlTableModel>

#include <QMessageBox>

// Констуктор класса Widget
Widget::Widget(QWidget *parent) :
        QWidget(parent),
        ui(new Ui::Widget)
{
    ui->setupUi(this);
    
    model = new QSqlTableModel;
    model->setTable("groups");
    model->select();
    
    ui->tableView->setModel(model);
}

Widget::~Widget()
{
    delete ui;
}

// Слот добавляет группу из текстового поля ui->lineEdit в модель
void Widget::on_pushButton_clicked()
{
    if (ui->lineEdit->text().length() == 0) return;
    
    int row = 0;
    
    model->insertRows(row, 1);
    
    QModelIndex col = model->index(row, 1);
    
    model->setData(col, ui->lineEdit->text());
    model->submitAll();
    
    ui->lineEdit->setText("");
}


// Слот удаляет группу из модели
void Widget::on_pushButton_2_clicked()
{
    // Вычисляем текущий индекс
    QModelIndex index = ui->tableView->currentIndex();
    
    // Если индекс валидный
    if (index.isValid())
    {
        // то удаляем строку в модели
        model->removeRow(index.row());
    }
}
