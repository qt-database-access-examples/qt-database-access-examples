#include <QtGui/QApplication>
#include "widget.h"
#include "../connect.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    if (!connect()) return 1;

    Widget w;
    w.show();

    return a.exec();
}
