#-------------------------------------------------
#
# Project created by QtCreator 2014-07-25T15:16:02
#
#-------------------------------------------------

QT       += core gui sql

TARGET = DB07_x
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    sqlhighlighter.cpp

HEADERS  += widget.h \
    sqlhighlighter.h \
    ../connect.h

FORMS    += widget.ui
