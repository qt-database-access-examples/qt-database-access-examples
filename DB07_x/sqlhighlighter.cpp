#include "sqlhighlighter.h"

SQLHighlighter::SQLHighlighter(QObject *parent) :
        QSyntaxHighlighter(parent)
{
}

void SQLHighlighter::highlightBlock(const QString &text)
{
    int pos = 0; 	
    
    QFont normalFont;
    normalFont.setFamily("Consolas");
    normalFont.setPointSize(14);
    
    QFont boldFont(normalFont);
    boldFont.setWeight(QFont::Bold);
    
    QTextCharFormat normalFormat;
    normalFormat.setFont(normalFont);
    normalFormat.setForeground(Qt::black);
    
    QTextCharFormat commandsFormat;
    commandsFormat.setFont(boldFont);
    commandsFormat.setForeground(Qt::blue);
    
    QTextCharFormat aggregationsFormat;
    aggregationsFormat.setFont(normalFont);
    aggregationsFormat.setForeground(Qt::green);
    
    QTextCharFormat numberFormat;
    numberFormat.setFont(normalFont);
    numberFormat.setForeground(Qt::red);
            
    QTextCharFormat stringsFormat;
    stringsFormat.setFont(normalFont);
    stringsFormat.setForeground(Qt::darkGreen);
    
    QTextCharFormat commentFormat;
    commentFormat.setFont(normalFont);
    commentFormat.setForeground(Qt::gray);
    
    setFormat (0, text.length(), normalFormat); 
    
    static const QRegExp commandsRegexp ("\\b(?:select|from|where|and|case|when|then|else|distinct"
                                         "|all|null|is|like|between|not|group|by|having|order|inner"
                                         "|outer|right|left|alter|with|isnull|cast|create|replace"
                                         "|function|returns|language|volatile|cost|table|view|or|"
                                         "join|on|using|union|exists|in|as|intersect|except|"
                                         "coalesce|insert|into|update)\\b", 
                                         Qt::CaseInsensitive);
    pos = 0;
    while ((pos = commandsRegexp.indexIn (text, pos)) != -1)	
    {
        setFormat (pos, commandsRegexp.matchedLength(), commandsFormat);
        pos += commandsRegexp.matchedLength();
    }
    
    static const QRegExp aggregationsRegexp ("\\b(?:count|min|max)\\b\\s*\\([^\\)]+\\)", 
                                             Qt::CaseInsensitive);
    pos = 0;
    while ((pos = aggregationsRegexp.indexIn (text, pos)) != -1)	
    {
        setFormat (pos, aggregationsRegexp.matchedLength(), aggregationsFormat);
        pos += aggregationsRegexp.matchedLength();
    }
    
    static const QRegExp numbersRegexp ("[^\\w]((\\d+)(\\.)?)",
                                        Qt::CaseInsensitive);
    pos = 0;
    while ((pos = numbersRegexp.indexIn (text, pos)) != -1)	
    {
        setFormat (pos, numbersRegexp.matchedLength(), numberFormat);
        pos += numbersRegexp.matchedLength();
    }
    
    static const QRegExp stringsRegexp ("'[^']+'", 
                                        Qt::CaseInsensitive);
    pos = 0;
    while ((pos = stringsRegexp.indexIn (text, pos)) != -1)	
    {
        setFormat (pos, stringsRegexp.matchedLength(), stringsFormat);
        pos += stringsRegexp.matchedLength();
    }
    
    pos = 0;
    static const QRegExp commentRegexp ("^\\s*(--)"); 	
    if ((pos = commentRegexp.indexIn (text, pos)) != -1)	
    {
        setFormat (pos, text.length(), commentFormat);
        return;	
    } 	
}
