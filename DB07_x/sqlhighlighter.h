#ifndef SQLHIGHLIGHTER_H
#define SQLHIGHLIGHTER_H

#include <QSyntaxHighlighter>

class SQLHighlighter : public QSyntaxHighlighter
{
    Q_OBJECT
public:
    explicit SQLHighlighter(QObject *parent = 0);

protected:
    void highlightBlock(const QString &text);

};

#endif // SQLHIGHLIGHTER_H
