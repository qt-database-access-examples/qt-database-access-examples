#include "widget.h"
#include "ui_widget.h"

#include <QSqlQueryModel>
#include <QSqlQuery>
#include <QSqlError>

// Констуктор класса Widget
Widget::Widget(QWidget *parent) :
        QWidget(parent),
        ui(new Ui::Widget)
{
    ui->setupUi(this);
    
    // Создаем экземпляр модели
    model = new QSqlQueryModel;
    // Устанавливаем модель для представления
    ui->view->setModel(model);
    // Настраиваем стиль отображения поля SQL
    ui->sqlEdit->setStyleSheet("font-family: consolas; font-size: 14pt;");
    // Скрываем поле для вывода ошибок
    ui->errorLabel->setVisible(false);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_queryButton_clicked()
{
    // Берем строку из поля SQL
    QString sql = ui->sqlEdit->toPlainText();
    
    // Создаем объект запроса QSqlQuery
    QSqlQuery query;
    // Передаем ему строку с запросом SQL
    query.exec(sql);
    
    // Проверяем наличие ошибки
    QSqlError err = query.lastError();
    // Если есть ошибка
    if (err.type() != QSqlError::NoError)
    {
        // Отображаем метку для ошибок
        ui->errorLabel->setVisible(true);
        // Выводим текст ошибки в метку 
        ui->errorLabel->setText( err.text() );
        
        // Очищаем модель 
        model->clear();
        // Очищаем представление
        ui->view->setModel(0);
        
        // Выходим из функции
        return;
    }
   
    // Если ошибки нет    
    // то скрываем метку для ошибки
    ui->errorLabel->setVisible(false);
    
    // Устанавливаем запрос в модель QSqlQueryModel
    model->setQuery(query);
    // Задаем модель представлению
    ui->view->setModel(model);
}
