-- Valentina Studio --
-- MySQL dump --
-- ---------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- ---------------------------------------------------------


-- CREATE DATABASE "students" ------------------------------
CREATE DATABASE IF NOT EXISTS `students` CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `students`;
-- ---------------------------------------------------------


-- CREATE TABLE "groups" -----------------------------------
DROP TABLE IF EXISTS `groups` CASCADE;

CREATE TABLE `groups` ( 
	`id_group` Int( 11 ) AUTO_INCREMENT NOT NULL, 
	`name_group` VarChar( 10 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	 PRIMARY KEY ( `id_group` )
 )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 8;
-- ---------------------------------------------------------


-- CREATE TABLE "studlist" ---------------------------------
DROP TABLE IF EXISTS `studlist` CASCADE;

CREATE TABLE `studlist` ( 
	`id_stud` Int( 11 ) AUTO_INCREMENT NOT NULL, 
	`fio` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
	`id_group` Int( 11 ) NOT NULL DEFAULT '1',
	 PRIMARY KEY ( `id_stud` )
 )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 10;
-- ---------------------------------------------------------


-- Dump data of "groups" -----------------------------------
BEGIN;

INSERT INTO `groups`(`id_group`,`name_group`) VALUES(1, 1011);
INSERT INTO `groups`(`id_group`,`name_group`) VALUES(2, 1021);
INSERT INTO `groups`(`id_group`,`name_group`) VALUES(3, 1031);
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "studlist" ---------------------------------
BEGIN;

INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Куликов В.В.', 1);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Лизогуб А.С.', 1);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Лунев А.С.', 1);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Мамедов С.Г.', 1);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Манеев О.Е.', 1);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Марков М.Е.', 1);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Мирзаев В.Н.', 1);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Носов В.С.', 1);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Нырков Н.Н.', 1);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Пашнин А.А.', 1);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Пестов А.С.', 1);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Петров В.С.', 1);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Прокопьев А.В.', 1);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Сазонова С.П.', 1);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Сеидов А.И.'С, 1);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Сибирцева Ю.А.', 1);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Тигай Р.А.', 1);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Тимофеев А.А.', 1);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Титов И.А.', 1);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Уласик А.Г.', 1);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Фарафонтов Ф.Н.', 1);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Фролов А.П.', 1);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Хоменко В.В.', 1);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Черняк П.А.', 1);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Шкарин С.А.', 1);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Печенкина А.А.', 1);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Похвала С.Ю.', 1);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Щукина Т.В.', 1);

INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Асламов Е.А.', 2);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Ахметов Е.А.', 2);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Бабинский Е.С.', 2);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Баховадинов О.Б.', 2);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Белкин И.А.', 2);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Билецкий И.В.', 2);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Веретин Д.И.', 2);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Виноградов Е.И.', 2);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Высотина Т.А.', 2);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Вяткин Д.Е.', 2);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Давыденков В.В.', 2);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Дарабан Л.', 2);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Долматов Р.М.', 2);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Железняков В.И.', 2);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Жиркова В.П.', 2);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Кантемирова Л.Д.', 2);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Каракуян В.Е.', 2);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Кириллов Р.А.', 2);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Кислицин И.С.', 2);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Климутко А.А.', 2);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Коваленко А.Е.', 2);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Конюх Д.А.', 2);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Копылова Ю.В.', 2); 
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Красников В.Е.', 2);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Альховский В.А.', 2);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Максимцова А.С.', 2);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Ермак И.А.', 2);

INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Абраимов А.А.', 3);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Абрамов М.С.', 3);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Баширов О.С.', 3);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Березко А.Е.', 3);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Васильцова Д.П.', 3);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Генералов П.С.', 3);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Журавский Е.А.', 3);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Каверин М.А.', 3);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Климашевский П.В.', 3);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Кравец В.А.', 3);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Красноштанов И.Д.', 3);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Михалдык С.Э.', 3);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Павловский А.А.', 3);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Супрун К.Ю.', 3);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Хандрымайлов А.Ю.', 3);
INSERT INTO `studlist`(`fio`, `id_group`) VALUES('Циркунов Е.Н.', 3);
COMMIT;
-- ---------------------------------------------------------


-- CREATE INDEX "FK__groups" -------------------------------
CREATE INDEX `FK__groups` USING BTREE ON `studlist`( `id_group` );
-- ---------------------------------------------------------


-- CREATE LINK "FK__groups" --------------------------------
ALTER TABLE `studlist` DROP FOREIGN KEY `FK__groups`;


ALTER TABLE `studlist` ADD CONSTRAINT `FK__groups` FOREIGN KEY ( `id_group` ) REFERENCES `groups`( `id_group` ) ON DELETE Restrict ON UPDATE Restrict;
-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------

