#include "widget.h"
#include "ui_widget.h"

#include <QSqlRelationalTableModel>
#include <QSortFilterProxyModel>

// Констуктор класса Widget
Widget::Widget(QWidget *parent) :
        QWidget(parent),
        ui(new Ui::Widget)
{
    ui->setupUi(this);
    
    studlistModel = new QSqlRelationalTableModel;
        
    studlistModel->setTable("studlist");
    
    studlistModel->setRelation(2, QSqlRelation("groups", "id_group", "name_group") ); 
    
    studlistModel->select();
    
    ui->studlistView->setModel(studlistModel);
    
    ui->studlistView->hideColumn(0);
    
    ui->studlistView->setEditTriggers ( QAbstractItemView::NoEditTriggers );
}

Widget::~Widget()
{
    studlistModel->submitAll();    
    delete ui;
}

void Widget::on_editStudent_textChanged(QString str)
{
    studlistModel->setFilter(QString("fio LIKE '%%1%'").arg(str));
}
