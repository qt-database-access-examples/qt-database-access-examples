#include "widget.h"
#include "ui_widget.h"
#include <QSqlRelationalTableModel>
#include <QSqlRelationalDelegate>

// Констуктор класса Widget
Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    // Создаем модель с отношениями
    model = new QSqlRelationalTableModel;

    // Задаем таблицу
    model->setTable("studlist");
    
    // Устанавливаем отношение
    model->setRelation(2, QSqlRelation("groups", "id_group", "name_group"));
    // Команда означает следующее:
    // Утановить соответствие для столбца №2 (studlist.id_group) и столбца groups.id_group
    // Заместо id_group подставлять name_group

    model->select();

    ui->tableView->setModel(model);
    
    // Задаем делегат для отображения списка при редактировании отношения
    ui->tableView->setItemDelegate(new QSqlRelationalDelegate(ui->tableView));
}

Widget::~Widget()
{
    // Перед разрушением виджета отправляем все данные в БД
    model->submitAll();
    
    delete ui;
}
