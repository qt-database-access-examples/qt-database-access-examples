#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

class QSqlRelationalTableModel; // Предварительное объявление класса QSqlTableModel

namespace Ui {
    class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private:
    Ui::Widget *ui;
    QSqlRelationalTableModel* model; // Включаем модель в состав виджета
};

#endif // WIDGET_H
