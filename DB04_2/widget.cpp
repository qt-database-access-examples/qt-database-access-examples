#include "widget.h"
#include "ui_widget.h"

#include <QSqlTableModel>
#include <QSqlRecord>
#include <QModelIndex>

// Констуктор класса Widget
Widget::Widget(QWidget *parent) :
        QWidget(parent),
        ui(new Ui::Widget)
{
    ui->setupUi(this);
    
    groupsModel = new QSqlTableModel;
    studlistModel = new QSqlTableModel;
    
    groupsModel->setTable("groups");
    studlistModel->setTable("studlist");
    
    studlistModel->setFilter("id_group = -1");
    
    groupsModel->select();
    studlistModel->select();
    
    ui->groupsView->setModel(groupsModel);
    ui->studlistView->setModel(studlistModel);
    
    ui->groupsView->hideColumn(0);
    ui->studlistView->hideColumn(0);
    ui->studlistView->hideColumn(2);
    
    connect(ui->groupsView->selectionModel(), SIGNAL(currentChanged(QModelIndex,QModelIndex)),
            this, SLOT(groupChanged(QModelIndex)));
}

Widget::~Widget()
{
    // Перед разрушением виджета отправляем все данные в БД
    groupsModel->submitAll();
    studlistModel->submitAll();
    
    delete ui;
}

void Widget::groupChanged(const QModelIndex &index)
{
    if (index.isValid())
    {
        QSqlRecord rec = groupsModel->record(index.row());
        int id_group = rec.value("id_group").toInt();
        QString filter = QString("id_group = %1").arg(id_group);
        studlistModel->setFilter(filter);
    } else
    {
        studlistModel->setFilter("id_group = -1");
    }
    studlistModel->select();
}

void Widget::on_buttonAddStudent_clicked()
{
    if (ui->editStudent->text().length() == 0) return;
    
    QModelIndex index = ui->groupsView->currentIndex();
    
    if (!index.isValid()) return;
    
    QSqlRecord rec = groupsModel->record(index.row());
    int id_group = rec.value("id_group").toInt();
    
    QString fio = ui->editStudent->text();
    
    int row = 0;
    studlistModel->insertRow(row);
    
    QModelIndex fioCol = studlistModel->index(row, 1);
    QModelIndex id_groupCol = studlistModel->index(row, 2);
    
    studlistModel->setData(fioCol, fio);    
    studlistModel->setData(id_groupCol, id_group);
    
    studlistModel->submitAll();
    
    ui->editStudent->setText("");
}

void Widget::on_buttonAddGroup_clicked()
{
    // Если поле ФИО пустое, выходим из функции
    if (ui->editGroup->text().length() == 0) return; 
    
    // Сохраняем ФИО из текстового поля в переменную group    
    QString group = ui->editGroup->text();
    
    // Добавляем строку в модель groupsModel
    int row = 0;
    groupsModel->insertRow(row);
    
    // Вычисляем индекс столбца group
    QModelIndex groupCol = groupsModel->index(row, 1);
    
    // Заносим данные в столбец group
    groupsModel->setData(groupCol, group);
    
    // Отправляем все данные в БД
    groupsModel->submitAll();
    
    // Очищаем поле
    ui->editGroup->setText("");
}
