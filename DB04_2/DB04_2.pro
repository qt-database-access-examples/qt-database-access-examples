#-------------------------------------------------
#
# Project created by QtCreator 2014-07-25T15:16:02
#
#-------------------------------------------------

QT       += core gui sql

TARGET = DB04_2
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp

HEADERS  += widget.h \
    ../connect.h

FORMS    += widget.ui
