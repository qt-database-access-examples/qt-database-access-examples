#include <QtGui/QApplication>

#include <QSqlDatabase>

#include <QSqlTableModel>
#include <QTableView>

#include <QHBoxLayout>

#include <QTextCodec>

#include <QMessageBox>

bool connect()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");

    db.setHostName("localhost");
    db.setPort(3306);

    db.setUserName("root");
    db.setPassword("usbw");

    db.setDatabaseName("students");

    return db.open();
}

int main(int argc, char *argv[])
{
    QTextCodec* codec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForCStrings(codec);
    QTextCodec::setCodecForLocale(codec);
    QTextCodec::setCodecForTr(codec);

    QApplication a(argc, argv);

    if (!connect())
    {
        QMessageBox::critical(0, "Ошибка", "Ошибка подключения к БД!");
        return 1;
    }

    QSqlTableModel* model = new QSqlTableModel;
    QTableView* view = new QTableView;

    model->setTable("studlist");
    model->select();

    view->setModel(model);

    // Заставляем представление выделять только одну строку
    view->setSelectionMode(QAbstractItemView::SingleSelection);
    view->setSelectionBehavior(QAbstractItemView::SelectRows);

    // Заставляем столбцы принять минимальный размер
    view->resizeColumnsToContents();

    // Скрываем столбцы №0 и №2
    view->setColumnHidden(0, true);
    view->setColumnHidden(2, true);

    view->show();

    return a.exec();
}


