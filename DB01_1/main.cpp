#include <QtGui/QApplication>

#include <QSqlDatabase>

#include <QSqlTableModel>
#include <QTableView>

#include <QTextCodec> // Заголовочный файл текстовых кодеков

#include <QMessageBox> // Для обображения сообщения об ошибке

bool connect()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");

    db.setHostName("localhost");
    db.setPort(3306);

    db.setUserName("root");
    db.setPassword("usbw");

    db.setDatabaseName("students");

    return db.open();
}

int main(int argc, char *argv[])
{   
    // Задаем тексовые кодеки для правильного отображения русского текста
    QTextCodec* codec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForCStrings(codec);
    QTextCodec::setCodecForLocale(codec);
    QTextCodec::setCodecForTr(codec);

    QApplication a(argc, argv);

    if (!connect())
    {
        // В случае неудачного подключения к БД выводим сообщение об ошибке
        QMessageBox::critical(0, "Ошибка", "Ошибка подключения к БД!");
        return 1;
    }

    QSqlTableModel* model = new QSqlTableModel;
    QTableView* view = new QTableView;

    model->setTable("studlist");
    model->select();

    // Задаем подписи столбцов
    model->setHeaderData(0, Qt::Horizontal, "Код");
    model->setHeaderData(1, Qt::Horizontal, "ФИО студента");
    model->setHeaderData(2, Qt::Horizontal, "Номер группы");

    view->setModel(model);

    view->show();

    return a.exec();
}

