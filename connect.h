#ifndef CONNECT_H
#define CONNECT_H

#include <QSqlDatabase>

bool connect()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");

    db.setHostName("localhost");
    db.setPort(3306);

    db.setUserName("root");
    db.setPassword("usbw");

    db.setDatabaseName("students");

    return db.open();
}

#endif // CONNECT_H
