#include "widget.h"
#include "ui_widget.h"
#include <QSqlTableModel>

#include <QMessageBox>

// Констуктор класса Widget
Widget::Widget(QWidget *parent) :
        QWidget(parent),
        ui(new Ui::Widget)
{
    ui->setupUi(this);
    
    model = new QSqlTableModel;
    model->setTable("groups");
    model->select();
    
    ui->tableView->setModel(model);
}

Widget::~Widget()
{
    delete ui;
}

// Слот добавляет группу из текстового поля ui->lineEdit в модель
void Widget::on_pushButton_clicked()
{
    if (ui->lineEdit->text().length() == 0) return;
    
    int row = 0;
    
    model->insertRows(row, 1);
    
    QModelIndex col = model->index(row, 1);
    
    model->setData(col, ui->lineEdit->text());
    model->submitAll();
    
    ui->lineEdit->setText("");
}

// Слот удаляет группу из модели
void Widget::on_pushButton_2_clicked()
{
    QModelIndex index = ui->tableView->currentIndex();
    if (index.isValid())
    {
        // Вызываем диалог с вопросом
        int ret = QMessageBox::critical(this, "",
                                        tr("Вы действительно хотите удалить группу?"),
                                        QMessageBox::Yes | QMessageBox::No,
                                        QMessageBox::No);
        // Если ответ диалога "Да"
        if (ret == QMessageBox::Yes)
            // то удаляем строку из модели
            model->removeRow(index.row());
    }
}
