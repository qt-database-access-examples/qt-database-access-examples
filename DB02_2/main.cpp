#include <QtGui/QApplication>
#include "widget.h"
#include "../connect.h"

#include <QTextCodec>

int main(int argc, char *argv[])
{
    QTextCodec* codec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForCStrings(codec);
    QTextCodec::setCodecForLocale(codec);
    QTextCodec::setCodecForTr(codec);
    
    QApplication a(argc, argv);

    if (!connect()) return 1;

    Widget w;
    w.show();

    return a.exec();
}
