#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

class QSqlTableModel; // Предварительное объявление класса QSqlTableModel

namespace Ui {
    class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private:
    Ui::Widget *ui;
    QSqlTableModel* model; // Включаем модель в состав виджета

private slots:
    void on_pushButton_clicked();
};

#endif // WIDGET_H
