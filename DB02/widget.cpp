#include "widget.h"
#include "ui_widget.h"
#include <QSqlTableModel>

// Констуктор класса Widget
Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    // Создаем модель
    model = new QSqlTableModel;

    // Задаем таблицу
    model->setTable("groups");

    // Выбираем таблицу из модели
    model->select();

    // Для представления tableView на виджете задаем модель
    ui->tableView->setModel(model);
}

Widget::~Widget()
{
    delete ui;
}

// Слот добавляет группу из текстового поля ui->lineEdit в БД в таблицу groups
void Widget::on_pushButton_clicked()
{
    // Если текстовое поле пустое
    // то выходим из функции
    if (ui->lineEdit->text().length() == 0) return;

    // Номер строки, с которой будем работать
    int row = 0;

    // Заставляем модель вставить пустую строку в позиции row
    model->insertRow(row);

    // Выбираем индекс столбца №1
    QModelIndex col = model->index(row, 1);

    // Берем текст из поля на форме ui->lineEdit
    // и помещаем ячейку по индексу col
    model->setData(col, ui->lineEdit->text());

    // Столбец №0 - это ключ
    // Если ключ по умолчанию имеет значение AUTO_INCREMENT, то его заполняет БД
    // иначе необходимо заполнять самостоятельно
    // Для это также необходимо получать индекс столбца и вызывать функцию setData

    // Заставляем модель отправить данные в БД
    model->submitAll();

    // Очищаем текстовое поле
    ui->lineEdit->setText("");
}
