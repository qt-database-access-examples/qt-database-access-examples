TEMPLATE = subdirs

SUBDIRS += \
    DB01    \
    DB01_1  \
    DB01_2  \
    DB02    \
    DB02_1  \
    DB02_2  \
    DB03    \
    DB04    \
    DB04_1  \
    DB04_2  \
    DB05    \
    DB05_1  \
    DB06    \
    DB06_1  \
    DB07    \
    DB07_1  \
    DB07_x
