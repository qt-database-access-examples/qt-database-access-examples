#include "widget.h"
#include "ui_widget.h"

#include <QSqlTableModel>
#include <QSqlRecord>
#include <QModelIndex>

// Констуктор класса Widget
Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    // Создаем модели
    groupsModel = new QSqlTableModel;
    studlistModel = new QSqlTableModel;

    // Задаем таблицы
    groupsModel->setTable("groups");
    studlistModel->setTable("studlist");
    
    // Выбираем данные
    groupsModel->select();
    studlistModel->select();
    
    // Устанавливаем для представлений соответствующие модели
    ui->groupsView->setModel(groupsModel);
    ui->studlistView->setModel(studlistModel);

    // Удаляем лишние столбцы из представлений
    ui->groupsView->hideColumn(0);
    ui->studlistView->hideColumn(0);
    ui->studlistView->hideColumn(2);
    
    // Соединяем сигнал currentChanged со слотом groupChanged
    // Слот groupChanged описан как закрытый стол класса Widget     
    connect(ui->groupsView->selectionModel(), SIGNAL(currentChanged(QModelIndex,QModelIndex)),
            this, SLOT(groupChanged(QModelIndex)));
}

Widget::~Widget()
{
    // Перед разрушением виджета отправляем все данные в БД
    groupsModel->submitAll();
    studlistModel->submitAll();
    
    delete ui;
}

// Слот получает индекс и по индексу изменяет фильтр для подчиненной таблицы
void Widget::groupChanged(const QModelIndex &index)
{
    // Проверяем индекс на валидность
    if (index.isValid())
    {
        // Если индекс валидный
        // По индексу выбираем из модели запись (строку данных)
        QSqlRecord rec = groupsModel->record(index.row());
        
        // Вычисляем значение id_group
        int id_group = rec.value("id_group").toInt();
        
        // Создаем строку, используемую в качестве фильтра
        QString filter = QString("id_group = %1").arg(id_group);
        
        // Устанавливаем фильтр для модели
        studlistModel->setFilter(filter);
    } else
    {
        // Если индекс невалидный
        // устанавливаем фильтр для несуществующего значения id_group
        // Таким образом, данные в представлении не отображаются
        studlistModel->setFilter("id_group = -1");
    }
    
    // После задания фильтра необходимо выбрать данные из БД
    studlistModel->select();
}
