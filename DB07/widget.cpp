#include "widget.h"
#include "ui_widget.h"

#include <QSqlQueryModel>
#include <QSqlQuery>
#include <QSqlError>

// Констуктор класса Widget
Widget::Widget(QWidget *parent) :
        QWidget(parent),
        ui(new Ui::Widget)
{
    ui->setupUi(this);
    
    // Создаем экземпляр модели QSqlQueryModel
    model = new QSqlQueryModel;
    
    // Создаем запрос на языке SQL
    QString sql = "select * from studlist";
    
    // Создаем объект запроса QSqlQuery
    QSqlQuery query;
    
    // Передаем ему строку с запросом SQL на выполнение
    query.exec(sql);
    
    // Устанавливаем запрос в модель QSqlQueryModel
    model->setQuery(query);
    
    // Устанавливаем модель для представления
    ui->view->setModel(model);
}

Widget::~Widget()
{
    delete ui;
}
