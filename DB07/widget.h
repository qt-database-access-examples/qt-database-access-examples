#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

class QSqlQueryModel; // Предварительное объявление класса QSqlTableModel

namespace Ui {
    class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private:
    Ui::Widget *ui;
    QSqlQueryModel* model;
};

#endif // WIDGET_H
