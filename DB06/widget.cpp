#include "widget.h"
#include "ui_widget.h"

#include <QSqlTableModel>
#include <QSqlRelationalTableModel>
#include <QSqlRelationalDelegate>
#include <QSortFilterProxyModel>
#include <QDataWidgetMapper>

// Констуктор класса Widget
Widget::Widget(QWidget *parent) :
        QWidget(parent),
        ui(new Ui::Widget)
{
    ui->setupUi(this);
    
    studlistModel = new QSqlRelationalTableModel;
    studlistModel->setTable("studlist");
    studlistModel->setRelation(2, QSqlRelation("groups", "id_group", "name_group") ); 
    studlistModel->select();
    ui->studlistView->setModel(studlistModel);
    ui->studlistView->hideColumn(0);
    ui->studlistView->setEditTriggers ( QAbstractItemView::NoEditTriggers );
    
    mapper = new QDataWidgetMapper;
    mapper->setModel(studlistModel);
    mapper->setItemDelegate(new QSqlRelationalDelegate(this));
    mapper->addMapping(ui->editStudent, 1);
    mapper->addMapping(ui->editGroup, 2);
    mapper->toFirst();
    
    connect(ui->studlistView->selectionModel(), SIGNAL(currentChanged(QModelIndex,QModelIndex)),
            mapper, SLOT(setCurrentModelIndex(QModelIndex)));
}

Widget::~Widget()
{
    studlistModel->submitAll();    
    delete ui;
}
