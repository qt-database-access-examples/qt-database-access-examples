#include "widget.h"
#include "ui_widget.h"

#include <QSqlRelationalTableModel>
#include <QSortFilterProxyModel>

// Констуктор класса Widget
Widget::Widget(QWidget *parent) :
        QWidget(parent),
        ui(new Ui::Widget)
{
    ui->setupUi(this);
    
    studlistModel = new QSqlRelationalTableModel;
    proxyModel = new QSortFilterProxyModel;
        
    studlistModel->setTable("studlist");
    
    studlistModel->setRelation(2, QSqlRelation("groups", "id_group", "name_group") ); 
    
    studlistModel->select();
    
    proxyModel->setSourceModel(studlistModel);
    
    proxyModel->setFilterKeyColumn(1);
    
    ui->studlistView->setModel(proxyModel);
    
    ui->studlistView->hideColumn(0);
    
    ui->studlistView->setEditTriggers ( QAbstractItemView::NoEditTriggers );
}

Widget::~Widget()
{
    studlistModel->submitAll();    
    delete ui;
}

void Widget::on_editStudent_textChanged(QString str)
{
    if (ui->checkBox->isChecked())
        proxyModel->setFilterCaseSensitivity(Qt::CaseSensitive);
    else
        proxyModel->setFilterCaseSensitivity(Qt::CaseInsensitive);

    proxyModel->setFilterFixedString(str);
}
