#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

class QSqlRelationalTableModel; // Предварительное объявление класса QSqlTableModel
class QSortFilterProxyModel;
class QModelIndex; // Предварительное объявление класса QModelIndex

namespace Ui {
    class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private:
    Ui::Widget *ui;
    QSqlRelationalTableModel* studlistModel; // Модель для таблицы studlist
    QSortFilterProxyModel* proxyModel;
    
private slots:
    void on_editStudent_textChanged(QString );
};

#endif // WIDGET_H
