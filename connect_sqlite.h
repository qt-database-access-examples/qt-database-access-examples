#ifndef CONNECT_H
#define CONNECT_H

#include <QSqlDatabase>;

bool connect()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");

    db.setDatabaseName("students.sqlite");

    return db.open();
}

#endif // CONNECT_H
