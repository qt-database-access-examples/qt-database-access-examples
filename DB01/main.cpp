#include <QtGui/QApplication>

#include <QSqlDatabase>

#include <QSqlTableModel>
#include <QTableView>

// Функция, обеспечивающая подключение к БД
bool connect()
{
    // Создание экземпляра QSqlDatabase
    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");

    // Задать подключению расположение сервера MySQL
    db.setHostName("localhost");
    db.setPort(3306);

    // Задать учетные данные на сервере MySQL
    db.setUserName("root");
    db.setPassword("usbw");

    // Выбрать имя БД
    db.setDatabaseName("students");

    // Открываем подключение и возвращаем из функции результат подключения
    return db.open();
}


int main(int argc, char *argv[])
{
    // Создаем экземпляр приложения
    QApplication a(argc, argv);

    // Пробуем подключиться
    // Если не получается - программа завершается с кодом 1
    if (!connect()) return 1;

    // Создаем экземпляр модели
    QSqlTableModel* model = new QSqlTableModel;
    // Создаем экземпляр представления
    QTableView* view = new QTableView;

    // Задаем модели имя таблицы
    model->setTable("studlist");
    // Выбираем таблицу
    model->select();

    // Задаем для представления необходимую модель
    view->setModel(model);

    // Отображаем представление
    view->show();

    // Запускаем приложение
    return a.exec();
}
